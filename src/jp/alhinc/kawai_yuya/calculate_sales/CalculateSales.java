package jp.alhinc.kawai_yuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {
	public static void main(String[] args) throws IOException {
	 	// 1~2 コマンドライン引数の数検証
		if(args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		
		BufferedReader br = null;
		Map<String, String> branchNameMap = new HashMap <String, String>();
		Map<String, Long> branchSalesMap = new HashMap <String, Long>();
		ArrayList<String> fileList = new ArrayList<String>();
		
		// ファイルの読み込み
		try {
			File file = new File(args[0], "branch.lst");  // 支店ファイルの読み込み
			
			// 3 支店定義ファイルの存在確認 existsメソッド
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line; 
			
			while((line = br.readLine()) != null ) {
				String[] branch = line.split(",", 0);
				
				// 4~11 支店番号が3桁かどうか、12.13 ストア配列の要素が2つか検証
				if(!branch[0].matches("^[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}else if(branch.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				
				branchNameMap.put(branch[0],branch[1]); //keyと値の取得
				branchSalesMap.put(branch[0],(long) 0); // value売り上げには初期値で0
			}
			
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		
		
		
		// ここからファイルの中身の検証
		try {
			File dr = new File(args[0]); // 売り上げファイルの読み込み
			File[] list = dr.listFiles(); 
			
			for(int i =0;i < list.length;i++) {
				String fileName =  list[i].getName(); //ファイルの名前を取得
				
				
				// 16~18 売り上げファイル名のフォーマット確認,ファイルかどうかも
				if(fileName.matches("^[0-9]{8}.rcd$") && list[i].isFile()) {
					 fileList.add(fileName); 
				 }
			}
			
			
			// 14.15 ファイル連番確認、 連番最大値ではないフォルダーが含まれているか検証
			 
			 for(int i=0;i < fileList.size()-1;i++) {
				 String fileNumber = fileList.get(i).substring(0,8); //ファイル名の1～8行目を抜き出し
				 String fileNumber2 = fileList.get(i+1).substring(0,8);
				 int fn = (int) Integer.parseInt(fileNumber);
				 int fn2 = (int) Integer.parseInt(fileNumber2);
				 
				 if(fn2 - fn != 1) {
					 System.out.println("売上ファイル名が連番になっていません");
					 return;
			     }
			 }
			
			String line; 
			for(int i=0; i<fileList.size(); i++) { // リストに入っているファイルの数繰り返す
					File file = new File(args[0], fileList.get(i)); //売り上げファイルの読み込み
					FileReader fr = new FileReader(file);
					br = new BufferedReader(fr);
					List<String> salesList = new ArrayList<String>();
					while((line = br.readLine()) != null) {
						salesList.add(line);
					}
					
					
					 //25.26 段落が二行以内か検証
					if(salesList.size() != 2) {
						 System.out.println(fileList.get(i)+"のフォーマットが不正です");
						return;
					}
					
					
					// 19~22 売り上げ金額のフォーマット確認
					if(!salesList.get(1).matches("^[0-9]*$")) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
					
					
					// 24 売上ファイルの支店コードが支店定義ファイルに存在しない場合 
					if(!branchNameMap.containsKey(salesList.get(0))) {
						System.out.println(fileList.get(i)+"の支店コードが不正です");
						return;
					}
					

					long total = branchSalesMap.get(salesList.get(0)) + Long.parseLong(salesList.get(1));

					// 23 支店別集計合計金額が10桁超えた
					long salesMax = 10000000000L;
					if(total >= salesMax) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					
					branchSalesMap.put(salesList.get(0),total);
					}
			
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		
		if(!print(args[0],branchSalesMap,branchNameMap,"branch.out")) {
			return;
		}
		
		
		
	}

	public static boolean print(String path, Map<String, Long> salesMap, Map<String, String> nameMap, String fileName) {
		BufferedWriter bw = null;
		try{
			
			File outfile = new File(path, fileName);
			FileWriter fr = new FileWriter(outfile);
			bw = new BufferedWriter(fr);
				for(String key : salesMap.keySet()) {
				  bw.write(key+","+ nameMap.get(key) +","+ salesMap.get(key));
				  bw.newLine(); // ニューライン（改行）
			    }
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally {
				if(bw != null) {
					try {
						bw.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		 return true;
	}
	
}







